/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 by ThingPulse, Daniel Eichhorn
 * Copyright (c) 2018 by Fabrice Weinberg
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * ThingPulse invests considerable time and money to develop these open source libraries.
 * Please support us by buying our products (and not the clones) from
 * https://thingpulse.com
 *
 */

 // Include the correct display library
 // For a connection via I2C using Wire include
 #include <Wire.h>  // Only needed for Arduino 1.6.5 and earlier
 #include "SSD1306Wire.h" // legacy include: `#include "SSD1306.h"`
 // or #include "SH1106Wire.h", legacy include: `#include "SH1106.h"`
 // For a connection via I2C using brzo_i2c (must be installed) include
 // #include <brzo_i2c.h> // Only needed for Arduino 1.6.5 and earlier
 // #include "SSD1306Brzo.h"
 // #include "SH1106Brzo.h"
 // For a connection via SPI include
 // #include <SPI.h> // Only needed for Arduino 1.6.5 and earlier
 // #include "SSD1306Spi.h"
 // #include "SH1106SPi.h"

// Include the UI lib
#include "OLEDDisplayUi.h"

// Include custom images

#include "images.h"
#include "Arduino.h"

// Use the corresponding display class:

// Initialize the OLED display using SPI
// D5 -> CLK
// D7 -> MOSI (DOUT)
// D0 -> RES
// D2 -> DC
// D8 -> CS
// SSD1306Spi        display(D0, D2, D8);
// or
// SH1106Spi         display(D0, D2);

// Initialize the OLED display using brzo_i2c
// D3 -> SDA
// D5 -> SCL
// SSD1306Brzo display(0x3c, D3, D5);
// or
// SH1106Brzo  display(0x3c, D3, D5);

// Initialize the OLED display using Wire library
SSD1306Wire  display(0x3c, 5, 4);
// SH1106Wire display(0x3c, D3, D5);

const int button1 = 13;
const int button2 = 16;
const int button3 = 12;
const int button4 = 15;
const int stickX = 39;
const int stickY = 36;
const int stickS = 25;



int playerPosX = 60;
int playerPosY = 30;
int playerTransitionX = 0;
int playerTransitionY = 0;

OLEDDisplayUi ui     ( &display );

void msOverlay(OLEDDisplay *display, OLEDDisplayUiState* state) {
  display->setTextAlignment(TEXT_ALIGN_RIGHT);
  display->setFont(ArialMT_Plain_10);
  display->drawString(128, 0, String(millis()));
  display->drawCircle(playerPosX + playerTransitionX, playerPosY + playerTransitionY, 5);
}

// First frame. Shows a logo
void drawFrame1(OLEDDisplay *display, OLEDDisplayUiState* state, int16_t x, int16_t y) {
  // draw an xbm image.
  // Please note that everything that should be transitioned
  // needs to be drawn relative to x and y
    playerTransitionX = x;
    playerTransitionY = y;

  display->drawXbm(x + 34, y + 14, Infa_Logo_width, Infa_Logo_height, Infa_Logo_bits);
}

// Second frame. Shows stars flying across the screen.
void drawFrame2(OLEDDisplay *display, OLEDDisplayUiState* state, int16_t x, int16_t y) {
  // Demonstrates the 3 included default sizes. The fonts come from SSD1306Fonts.h file
  // Besides the default fonts there will be a program to convert TrueType fonts into this format
//  display->setTextAlignment(TEXT_ALIGN_LEFT);
//  display->setFont(ArialMT_Plain_10);
//  display->drawString(0 + x, 10 + y, "Arial 10");

//  display->setFont(ArialMT_Plain_16);
//  display->drawString(0 + x, 20 + y, "Arial 16");

//  display->setFont(ArialMT_Plain_24);
//  display->drawString(0 + x, 34 + y, "Arial 24");

//backup ifall allt går åt skogen
//  display->drawXbm(+x + 64, +x + 9, Star_width, Star_height, Star_bits);
//  display->drawXbm(+x + 26, -x + 35, Star_width, Star_height, Star_bits);
//  display->drawXbm(+x + 47, +x + 25, Star_width, Star_height, Star_bits);
//  display->drawXbm(-x + 14, -3*x + 9, Star_width, Star_height, Star_bits);
//  display->drawXbm(-x + 94, +x + 14, Star_width, Star_height, Star_bits);
//  display->drawXbm(-x + 107,-x/2 + 33, Star_width, Star_height, Star_bits);
  playerTransitionX = x;
  playerTransitionY = y;

  display->drawXbm(+(x+2*y) + 32, +(x+y) - 15, Star_width, Star_height, Star_bits);
  display->drawXbm(+(x+2*y) - 8, -(x+y) + 25, Star_width, Star_height, Star_bits);
  display->drawXbm(+(x+2*y) + 16, +(x+y) + 20, Star_width, Star_height, Star_bits);
  display->drawXbm(-(x+2*y) - 16, -3*(x+y) - 10, Star_width, Star_height, Star_bits);
  display->drawXbm(-(x+2*y) + 64, +(x+y) - 7, Star_width, Star_height, Star_bits);
  display->drawXbm(-(x+2*y) + 67,-(x+y)/2 + 23, Star_width, Star_height, Star_bits);
  ui.setTimePerFrame(0);
}

// Third frame. The title screen.
void drawFrame3(OLEDDisplay *display, OLEDDisplayUiState* state, int16_t x, int16_t y) {
  // Text alignment demo
  playerTransitionX = x;
  playerTransitionY = y;

  ui.setTimePerFrame(5000);
  display->setFont(ArialMT_Plain_16);

  // The coordinates define the left starting point of the text
  display->setTextAlignment(TEXT_ALIGN_CENTER);
  display->drawString(64 + x, 11 + y, "Tape Hero");

  display->setFont(ArialMT_Plain_10);

  // The coordinates define the center of the text
  display->setTextAlignment(TEXT_ALIGN_LEFT);
  display->drawString(0 + x, 27 + y, "The epic story game");

  // The coordinates define the right end of the text
  display->setTextAlignment(TEXT_ALIGN_RIGHT);
  display->drawString(128 + x, 43 + y, "Play ->");
}

// Fourth frame. Some lore for the game...
void drawFrame4(OLEDDisplay *display, OLEDDisplayUiState* state, int16_t x, int16_t y) {
  // Demo for drawStringMaxWidth:
  // with the third parameter you can define the width after which words will be wrapped.
  // Currently only spaces and "-" are allowed for wrapping
  playerTransitionX = x;
  playerTransitionY = y;

  display->setTextAlignment(TEXT_ALIGN_LEFT);
  display->setFont(ArialMT_Plain_10);
  display->drawStringMaxWidth(0 + x, 8 + y, 128, "The story begins in an old dark forest. The hero has run out of tape and dangerous wolves roam...");
}

// Fifth frame. The end.
void drawFrame5(OLEDDisplay *display, OLEDDisplayUiState* state, int16_t x, int16_t y) {
  playerTransitionX = x;
  playerTransitionY = y;

  display->setTextAlignment(TEXT_ALIGN_CENTER);
  display->setFont(ArialMT_Plain_24);
  display->drawString(64 + x, 17 + y, "Fin");
  display->setPixel(10 + x, 10 + y);
  display->drawLine(45 + x,42 + y,83 + x,42 + y);
}

// This array keeps function pointers to all frames
// frames are the single views that slide in
FrameCallback frames[] = { drawFrame1, drawFrame2, drawFrame3, drawFrame4, drawFrame5 };

// how many frames are there?
int frameCount = 5;

// Overlays are statically drawn on top of a frame eg. a clock
OverlayCallback overlays[] = { msOverlay };
int overlaysCount = 1;

void setup() {
  Serial.begin(115200);
  Serial.println();
  Serial.println();

  // Initialize pin input
  pinMode(button1, INPUT_PULLUP);
  pinMode(button2, INPUT_PULLUP);
  pinMode(button3, INPUT_PULLUP);
  pinMode(button4, INPUT_PULLUP);
  pinMode(stickX, INPUT_PULLUP);
  pinMode(stickY, INPUT_PULLUP);
  pinMode(stickS, INPUT_PULLUP);

	// The ESP is capable of rendering 60fps in 80Mhz mode
	// but that won't give you much time for anything else
	// run it in 160Mhz mode or just set it to 30 fps
  ui.setTargetFPS(60);

	// Customize the active and inactive symbol
  ui.setActiveSymbol(activeSymbol);
  ui.setInactiveSymbol(inactiveSymbol);

  // You can change this to
  // TOP, LEFT, BOTTOM, RIGHT
  ui.setIndicatorPosition(BOTTOM);

  // Defines where the first frame is located in the bar.
  ui.setIndicatorDirection(LEFT_RIGHT);

  // You can change the transition that is used
  // SLIDE_LEFT, SLIDE_RIGHT, SLIDE_UP, SLIDE_DOWN
  ui.setFrameAnimation(SLIDE_LEFT);

  // Add frames
  ui.setFrames(frames, frameCount);

  // Add overlays
  ui.setOverlays(overlays, overlaysCount);

  ui.setTimePerFrame(5000);
  ui.setTimePerTransition(1000);

  ui.disableAutoTransition();
  // Initialising the UI will init the display too.
  ui.init();

  //display.flipScreenVertically();

}


void loop() {
  int remainingTimeBudget = ui.update();

  if (remainingTimeBudget > 0) {

    // Button presses
    if (digitalRead(button2) == 0) {
      Serial.println("Knapp 2");
      ui.setFrameAnimation(SLIDE_LEFT);
      ui.nextFrame();
    } else {
      ui.setTimePerFrame(5000);
    }
    if (digitalRead(button1) == 0) {
      Serial.println("Knapp 1");
      ui.setFrameAnimation(SLIDE_LEFT);
      ui.previousFrame();
    }
    if (digitalRead(button3) == 0) {
      Serial.println("Knapp 3");
      ui.setFrameAnimation(SLIDE_UP);
      ui.nextFrame();
    }
    if (digitalRead(button4) == 0) {
      Serial.println("Knapp 4");
      ui.setFrameAnimation(SLIDE_UP);
      ui.previousFrame();
    }
    if (digitalRead(stickS) == 0) {
      Serial.println("\"Sad noises\"");
    }

    // Player movement
    if (analogRead(stickX) < 500) {
      playerPosX -= 1;
    } else if (analogRead(stickX) > 3500) {
      playerPosX += 1;
    }
    if (analogRead(stickY) < 500) {
      playerPosY += 1;
    } else if (analogRead(stickY) > 3500) {
      playerPosY -= 1;
    }

    // Scene transition
    if (playerPosX > 128) {
      ui.setFrameAnimation(SLIDE_LEFT);
      ui.nextFrame();
      playerPosX = 0;
    } else if (playerPosX < 0) {
      ui.setFrameAnimation(SLIDE_LEFT);
      ui.previousFrame();
      playerPosX = 128;
    } else if (playerPosY > 64) {
      ui.setFrameAnimation(SLIDE_UP);
      ui.nextFrame();
      playerPosY = 0;
    } else if (playerPosY < 0) {
      ui.setFrameAnimation(SLIDE_UP);
      ui.previousFrame();
      playerPosY = 64;
    }
    // You can do some work here
    // Don't do stuff if you are below your
    // time budget.
    delay(remainingTimeBudget);
  }
}
