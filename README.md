# Tape Hero

The start of a game built for the ESP32. This was just a hobby project and development has stopped.

The code is based on a sample project made for the ESP32 with changes added.

## Images

![alt text](images/Image3.jpeg "logo screen")
![alt text](images/Image1.jpeg "title screen")
![alt text](images/Image4.jpeg "Stars")
![alt text](images/Image2.jpeg "Stars moving")

![alt text](images/Video.mp4 "Video")
